var express = require('express');
var app = express();

var server_url = "127.0.0.1";
var server_port = 3001;
var io = require('socket.io').listen(3002);
var tty = require("serialport");
var fs = require('fs');


if (typeof localStorage === "undefined" || localStorage === null) {
	var LocalStorage = require('node-localstorage').LocalStorage;
	localStorage = new LocalStorage('./localstorage', 50 * 1024 * 1024); //50mb limit
	//localStorage._deleteLocation('./localstorage');  //cleans up ./localstorage created during doctest
	//localStorage.setItem('myFirstKey', JSON.stringify({test: 123.0})); 
	//console.log(localStorage.getItem('myFirstKey'));
}

app.use(express.urlencoded());
app.use('/assets', express.static('assets'));
app.get('/', function (req, res) {
	var path = req.params[0] ? req.params[0] : 'index.html';
   //res.sendFile(path, {root: './assets'});
   res.redirect('/assets/index.html');
});

app.get('/getKeys', function(req, res){
	var keys = new Array();
	for(var i=0; i<localStorage.length;i++){
		keys.push(localStorage.key(i));
	}
	res.write(JSON.stringify({keys: keys}));
	res.end();
});
app.get('/getItem', function(req, res){
	var key = req.query.key;
	//console.log("key: "+key);
	var item = localStorage.getItem(key);
	//console.log(item);
	res.write(item);
	res.end();
});

app.get('/saveItem', function(req, res){
	var key = req.query.key;
	var data = req.query.data;
	//console.log("key: "+key);
	//console.log(data);	
	localStorage.setItem(key, JSON.stringify(data));
	res.write('{"response": "success"}');
	res.end();
});
app.post('/', function(req, res){
	//console.log('Post recieved at http://%s:%s', server_url, server_port);
	//print(request, response);
	//console.log(req.query);
	//console.log(req.params);
	//console.log(request.originalUrl);
	//console.log(req.body);
	console.log(req.body.tag+": "+req.body.message);
		io.sockets.emit('log', {
         tag: req.body.tag,
         message: req.body.message
      });
   res.send({
   	status_code: 1,
   	message: ""
   });	
});
var server = app.listen(server_port, function () {
	  var host = server.address().address;
	  var port = server.address().port;

	  console.log('Example app listening at http://%s:%s', host, port);
});

//now the socketio server
var serialPort = null;
tty.list(function (err, ports) {
  console.log("#ports: "+ports.length);
  /*ports.forEach(function(port) {
    console.log(port.comName);
    console.log(port.pnpId);
    console.log(port.manufacturer);
  });*/
  if(ports.length > 0){
  		serialPort = new tty.SerialPort(ports[1].comName, {
  			baudrate: 115200,
			bufferSize: 4096
		});
		serialPort.on("open", function () {
  			serialPort.on('data', function(data) {
    			var a = data.toString().split(/\n/);
				//arduino: V0.00I-0.04AH0.00
    			for(var i=0;i<a.length;i++){
    				if(a[i].length > 0){
    					//console.log("arduino: "+a[i]); //log volts & amps
    					io.sockets.emit('log', {
							tag: "info",
							message: "arduino: "+a[i]
    					});
    				}
    			}

  				//serialPort.write("ls\n", function(err, results) {
  				//  console.log('err ' + err);
  				//  console.log('results ' + results);
  				//});
			});
		});
	} else {
  		serialPort = new tty.SerialPort("/dev/rfcomm0", {
  			baudrate: 115200
		});
		serialPort.on("open", function () {
  		//console.log('open');
  		serialPort.on('data', function(data) {
    		console.log('bluetooth: ' + data);
    		io.sockets.emit('log', {
         		tag: "info",
         		message: data+""
    			});    
  			});
  			//serialPort.write("ls\n", function(err, results) {
  			//  console.log('err ' + err);
  			//  console.log('results ' + results);
  			//});
		});		
	}
});	

io.on('connection', function (socket) {
	console.log("new connection...");

    socket.emit('log', {
        tag: "debug",
        message: "begin session..."
    });
});

process.on('SIGINT', function() {    
	 if(serialPort != null && serialPort.isOpen()){
	 	serialPort.close();
	 	console.log("Closing Com Port...");
	 }
    process.exit();
});
